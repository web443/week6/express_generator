const mongoose = require('mongoose')
const { Schema } = mongoose
const productSchema = Schema({
  username: String,
  password: String, // plain text
  roles: {
    type: [String],
    default: [USER]
  }
})

module.exports = mongoose.model('User', productSchema)
